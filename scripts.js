const app = document.getElementById('root');

const logo = document.createElement('img');
logo.src = 'https://upload.wikimedia.org/wikipedia/pt/f/f2/CatarinenseSicoob2019.png';

const container = document.createElement('div');
container.setAttribute('class', 'container');

app.appendChild(logo);
app.appendChild(container);

var request = new XMLHttpRequest();
request.open('GET', 'https://bitbucket.org/gabrielscoelho/imagine-json/raw/07b60f719b7ebacb41640e1697f4ed1b5ba7883a/times.json', true);
request.onload = function () {

  var data = JSON.parse(this.response);
  if (request.status >= 200 && request.status < 400) {
    data.forEach(time => {
      const card = document.createElement('div');
      card.setAttribute('class', 'card');

      const h1 = document.createElement('h1');
      h1.textContent = time.nome;

      const p = document.createElement('p');
      time.description = time.intro.substring(0, 300);
      p.textContent = `Fundação: ${time.criado} | De: ${time.cidade} | Apelido: ${time.apelido} | Estádio: ${time.estádio} | Títulos do catarinense: ${time.Títulos} | ${time.intro} `;

      container.appendChild(card);
      card.appendChild(h1);
      card.appendChild(p);
    });
  } 
}

request.send();